<p align="center"><img src="/screen.png" width="400" /></p>

# Burn Extension 


![](/src/icons/icon-128.png)

Burn Extension is a browser extension that is meant to aid your day-to-day life in terms of cutting down on tabs you need to open to check $ASH prices, burn ratios, or even $ETH or gas prices. 
All API calls route to my other project [**WatchCubesBurn.art**](https://watchcubesburn.art) which is directly pulling from Uniswap v3 and Gemini for $ASH and $ETH prices, uses CoinGecko's API to get the liquidity stats required for Burn metrics, and taps into the Etherscan API for gas prices.

Features:

- $ASH Price in **Uniswap v3** pool - ASH-to-ETH - Price of 1 ETH
- $ASH Price on **Gemini**'s site - ASH-to-ETH - Price of 1 ETH

- **Burn.art** High Tier burn rate, direct value, FYI, this does not take gas prices, slippage or other fees into consideration(as of yet), and Total count of ASH Holders.

- Gas prices via **Etherscan**

The badge is displaying the currently selected metrics, but defaults to Uniswap v3's ASH price - you can change it by clicking on a different metrics. 

All metrics are refreshed every 30/60s, depending on the browser type, or can be refreshed by clicking the $ASH logo in the upper right corner. 


## Privacy & Security

The extension has no tracking built in, the only permissions required by the extension are "alarms" and "storage".

## Extension links


- Chromium based extension: [Download for Chrome/Brave/Edge etc.](https://chrome.google.com/webstore/detail/burn-extension/gngcjimdeadmimigfkjhfjdmbfpipepk)
- Firefox extension: [Download for Firefox](https://addons.mozilla.org/en-CA/firefox/addon/burn-extension/)

## Build yourself 

First of all, you need npm to build the package, so check how to install it on your computer. i.e: (https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

**Chromium based:**
```javascript 
git clone  https://gitlab.com/secondstatebuilds/burnextension.git
cd burnextension
npm install
npm run build
```
You'll get a burnext.zip which you can then load into Chrome/Brave etc. with proper safechecks turned off OR you can just load the unpacked folder in fairly easily.
i.e:
https://webkul.com/blog/how-to-install-the-unpacked-extension-in-chrome/

**Firefox:**

You'll need web-ext : https://github.com/mozilla/web-ext

```javascript
git clone  https://gitlab.com/secondstatebuilds/burnextension.git
cd burnextension
npm install
npm run buildff
```
Output will be in the firefox folder.

You'll get a burn-extension[version].zip which you can then load into Firefox. with proper safechecks turned off OR you can just load the unpacked folder in fairly easily.
i.e:
https://extensionworkshop.com/documentation/develop/temporary-installation-in-firefox/

# Massive Thank You

- To Pak, for creating $ASH, and really just everything!
- To the community that revolves around Pak and the good hearted NFT community in general, it's truly something heart-warming to receive all the great feedback on the things I've been building. Love you all.
- Dude23 from Discord for the question if there's any extension to see stats for $ASH :) 
- Philippe who made https://github.com/philippe-git/ethereum-gas-prices-browser-extension, which heavily influenced this project, since I wanted to get this out as soon as possible :) 

## License 

MIT

## More info about the Burn mechanism, $ASH, and Pak in general
https://unofficialpak.xyz

## Contact
https://twitter.com/sec0ndstate
