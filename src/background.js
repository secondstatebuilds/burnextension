/* eslint-disable no-use-before-define */

const DECIMALS_WEI = 1e18;
const DECIMALS_GWEI = 1e9;

chrome.alarms.onAlarm.addListener(async ({ name }) => {
  if (name === 'fetch-gas') fetchGas();
  if (name === 'fetch-ash-prices') fetchASHPrices();
  if (name === 'fetch-burn') fetchBurn();
});

chrome.storage.onChanged.addListener(async (changes, areaName) => {
  if (areaName === 'local' && changes.prices) {
    const prices = changes.prices.newValue;
    const badgeSource = await getStoredBadgeSource();

    updateBadgeValue({ prices, badgeSource });
  }

  if (areaName === 'local' && changes.badgeSource) {
    const prices = await getStoredPrices();
    const badgeSource = changes.badgeSource.newValue;

    updateBadgeValue({ prices, badgeSource });
  }
});

const updateBadgeValue = ({ prices, badgeSource }) => {
  const [preferredProvider, preferredSpeed] = badgeSource.split('|');

  const value = (
    prices[preferredProvider][preferredSpeed] ||
    prices.gaspriceio[preferredSpeed] ||
    prices.ashgemini[preferredSpeed] ||
    prices.ash[preferredSpeed] ||
    prices.burn[preferredSpeed]
  );

  if (value) {
    chrome.browserAction.setBadgeText({
      text: `${Math.trunc(value)}`,
    });
  }
};

const sleep = (duration) => new Promise((resolve) => setTimeout(resolve, duration));
const lock = {
  state: {
    isLocked: false,
  },

  // Wait and acquire lock
  async acquire() {
    while (this.state.isLocked) {
      await sleep(100); // eslint-disable-line no-await-in-loop
    }

    this.state.isLocked = true;
  },

  release() {
    this.state.isLocked = false;
  },
};

const getStoredBadgeSource = () => new Promise((res) => {
  chrome.storage.local.get(['badgeSource'], (result) => {
    const defaultBadgeSource = 'ash|0';
    res((result && result.badgeSource) || defaultBadgeSource);
  });
});

const setStoredBadgeSource = async (badgeSource) => new Promise((res) => {
  chrome.storage.local.set({ badgeSource }, () => res());
});

const getStoredPrices = () => new Promise((res) => {
  chrome.storage.local.get(['prices'], (result) => {
    res({
      gaspriceio: (result && result.prices && result.prices.gaspriceio) || [null, null, null],
      ashgemini: (result && result.prices && result.prices.ashgemini) || [null, null, null],
      ash: (result && result.prices && result.prices.ash) || [null, null, null],
      burn: (result && result.prices && result.prices.burn) || [null, null, null],
    });
  });
});

const setStoredPrices = async (prices) => new Promise((res) => {
  chrome.storage.local.set({ prices }, () => res());
});

const saveFetchedPricesForProvider = async (source, prices) => {
  await lock.acquire();

  const storedPrices = await getStoredPrices();
  const timestamp = Math.trunc(+Date.now() / 1000);

  await setStoredPrices({
    ...storedPrices,
    [source]: prices.concat(timestamp),
  });

  lock.release();
};

const fetchGas = async () => {
  fetchgaspriceioData()
    .catch(() => [null, null, null]) // Default to null if network error
    .then((prices) => saveFetchedPricesForProvider('gaspriceio', prices));
};

const fetchBurn = async () => {
  fetchBurnData()
    .catch(() => [null, null, null]) // Default to null if network error
    .then((prices) => saveFetchedPricesForProvider('burn', prices));
};

const fetchASHPrices = async () => {
  fetchASHGemini()
    .catch(() => [null, null, null]) // Default to null if network error
    .then((prices) => saveFetchedPricesForProvider('ashgemini', prices));

  fetchASHData()
    .catch(() => [null, null, null]) // Default to null if network error
    .then((prices) => saveFetchedPricesForProvider('ash', prices));
};



const fetchgaspriceioData = async () => {
  const { result: { instant, fast, eco, baseFee } } =
    await (await fetch('https://api.gasprice.io/v1/estimates') ).json();

  return [
    parseInt(eco.feeCap, 10),
    parseInt(fast.feeCap, 10),
    parseInt(instant.feeCap, 10)
    
    
  ];
};


const fetchASHData = async () => {
  const {data: { bundle, token }}  = await (await fetch('https://api.thegraph.com/subgraphs/name/uniswap/uniswap-v3', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
    query: `{
      token(id:"0x64d91f12ece7362f91a6f8e7940cd55f05060b92") {
          symbol
          derivedETH
        
      }
      bundle(id: "1" ) {   ethPriceUSD }
    }
`}
    ),
  })).json();


  const ashprice = bundle.ethPriceUSD * token.derivedETH;

    return [
      ashprice.toFixed(2),
      token.derivedETH.toString().substring(0, 6),
      parseInt(bundle.ethPriceUSD, 10).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
      
    ];
  
};

const fetchASHGemini = async () => {
    const data = await (await fetch('https://api.gemini.com/v1/pricefeed')).json();
    // const ashindex = data.findIndex(({ pair }) => pair === 'ASHUSD');
    // const ethindex = data.findIndex(({ pair }) => pair === 'ETHUSD');
    const ashask = data.find(({ pair }) => pair === 'ASHUSD').price;
    // const ashChange = data.priceStats.ASHUSD.change24hours;
    // const ashmin = data.priceStats.ASHUSD.min24hours
    // const ashPercentageChange = (ashChange / ashmin * 100)

    // const ethlast = priceStats.ETHUSD.lastPrice
    const ethask =  data.find(({ pair }) => pair === 'ETHUSD').price;
    // const ethChange = priceStats.ETHUSD.change24hours
    
    
  
      return [
           (ashask).toString().substring(0, 5),
          (ashask / ethask).toFixed(4),
          (ethask).toString().substring(0, 4).replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        // ashmin.toFixed(2),
        //   ashmin,
        //   ashmin,
        // ashChange.toFixed(2),
        // ethlast.toFixed(2),
        
      ];
    
  };
  //if data is not available, return previous data that is available
  const fetchBurnData = async () => {
    const data  =
      await (await fetch('https://api.ethplorer.io/getTokenInfo/0x64d91f12ece7362f91a6f8e7940cd55f05060b92?apiKey=freekey')).json();
      const totalSupplyRound  = Math.round(data.totalSupply / 10**18)

    return [
        ((0.5**(totalSupplyRound/5000000))*1000).toFixed(2), 
        data.holdersCount.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        (((0.5**(totalSupplyRound/5000000))*1000).toFixed(3) * await (await fetchASHData()).at(0)).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
      
    ];
  };

chrome.alarms.create('fetch-gas', { periodInMinutes: 0.5 });
fetchGas(); // Not using the `when` option for the alarm because Firefox doesn't run it
chrome.alarms.create('fetch-ash-prices', { periodInMinutes: 0.5 });
fetchASHPrices(); // Not using the `when` option for the alarm because Firefox doesn't run it
chrome.alarms.create('fetch-burn', { periodInMinutes: 2 });
fetchBurn(); // Not using the `when` option for the alarm because Firefox doesn't run it

// Set initial properties when the extension launches; since this isn't
// a persistent background script, it may be regularly shut down and initialized
// again: testing for the value of text allows to only apply initia value on the
// first initialization
chrome.browserAction.getBadgeText({}, (text) => {
  const isInitialRun = text === '';
  if (isInitialRun) chrome.browserAction.setBadgeText({ text: '…' });
});
chrome.browserAction.setBadgeBackgroundColor({ color: '#000' });

chrome.runtime.onMessage.addListener(({ action, ...data } = {}) => {
  if (action === 'refresh-data') fetchGas();
  if (action === 'refresh-data') fetchASHPrices();
  if (action === 'update-badge-source') setStoredBadgeSource(data.badgeSource);
});
